﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Polar_Cycle;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Convert_any_value_to_16_bits_binary()
        {
            int value = 10;
            string result = AdvancedMetrics.ToBinary(value);
            Assert.AreEqual(16, result.Length);
        }
    }
}
