﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polar_Cycle
{
    public class AdvancedMetrics
    {
        public static string ToBinary(int value, int length = 16)
        {
            return (length > 1 ? ToBinary(value >> 1, length - 1) : null) + "01"[value & 1];
        }

        public static double NormalisedPower(List<double> powerList, int interval)
        {
            var powerCount = powerList.Count();
            var toTake = 30 / interval;

            List<double> rollingPowers = new List<double>();

            for (var i = 0; i <= (powerCount - toTake); i++)
            {
                var pd = powerList.Skip(i).Take(toTake).Average();

                rollingPowers.Add(pd);
            }

            if (rollingPowers.Count >= 1)
            {
                List<double> powerFourth = new List<double>();

                foreach (var rollingPower in rollingPowers)
                {
                    powerFourth.Add(Math.Pow(rollingPower, 4));
                }


                var powerFourthAverage = powerFourth.Average();
                var normalisedPower = Math.Pow(powerFourthAverage, 1.0 / 4);

                normalisedPower = Math.Round(normalisedPower, 2);

                return normalisedPower;
            }

            return 0;
        }

        public static double IntensityFactor(double normalisedPower, int functionalThresholdPower)
        {
            var iF = normalisedPower / functionalThresholdPower;
            iF = Math.Round(iF, 2);

            return iF;
        }

        public static double TrainingStressScore(double seconds, double normalisedPower, double intensityFactor, int functionalThresholdPower)
        {
            var tss = (seconds * normalisedPower * intensityFactor) / (functionalThresholdPower * 3600) * 100;

            tss = Math.Round(tss, 2);

            return tss;
        }

        public static List<InterValDetector> InterValDetector(List<double> powerList, int minimumDuration)
        {
            var intervalDetectors = new List<InterValDetector>();
            int count = 0;
            bool isStart = true;
            var intervalDetector = new InterValDetector();

            var intervalIdentifierValue = Math.Round(powerList.Average(), 1);
            foreach (var power in powerList)
            {
                if (power >= intervalIdentifierValue && isStart)
                {
                    isStart = false;
                    intervalDetector.starting = count;
                }
                else if (power < intervalIdentifierValue &&
                        !isStart)
                {
                    intervalDetector.ending = count;
                    intervalDetector.time = (intervalDetector.ending.Value - intervalDetector.starting.Value);
                    if (intervalDetector.time > minimumDuration)
                    {
                        intervalDetectors.Add(intervalDetector);
                    }

                    intervalDetector = new InterValDetector();
                    isStart = true;
                }
                count++;
            };

            return intervalDetectors;
        }
    }
}
