﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polar_Cycle
{
    public class FileReader
    {
        private string filePath;
        OpenFileDialog path = new OpenFileDialog();
        public Dictionary<string, string> paramsData;
        public string smode;
        public List<double> heartRate;
        public List<double> speedInKilos;
        public List<double> speedInMiles;
        public List<double> cadence;
        public List<double> altitudeInMeters;
        public List<double> altitudeInFeet;
        public List<double> power;
        public List<double> powerBalanceAndPedalling;
        private string[] lines;

        public char[] smodeIndex;

        public DataTable dataTable;

        //class constructor
        public FileReader(string filePath)
        {
            this.filePath = filePath;
        }

        //gets and returns smode data
        public string SMode() {

            foreach (var line in Read()["[Params]"])
            {
                string[] parts = Regex.Split(line, "=");

                if (parts[0].Equals("SMode"))
                {
                    smode = parts[1];
                }
            }
            return smode;
        }

        //reads file and returns revelant data
        public Dictionary<string, List<string>> Read()
        {
            lines = File.ReadAllLines(filePath);
            var fileData = new Dictionary<string, List<string>>();

            foreach (var line in lines)
            {
                if (line.StartsWith("[") && line.EndsWith("]"))
                {
                    fileData.Add(line, new List<string>());
                }
                else
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        fileData.LastOrDefault().Value.Add(line);
                    }
                }
            }
            return fileData;
        }

        //places all params in a dictionary
        public Dictionary<string, string> ParamsToList()
        {
            paramsData = new Dictionary<string, string>();

            foreach(var line in Read()["[Params]"])
            {
                string[] parts = Regex.Split(line, "=");
                paramsData.Add(parts[0], parts[1]);

                if (parts[0].Equals("SMode")) {
                    smode = parts[1];
                    
                }
            }
            return paramsData;
        }

        //gets and returns all HR data
        public void HrDatatoLists()
        {
            heartRate = new List<double>();
            speedInKilos = new List<double>();
            speedInMiles = new List<double>();
            cadence = new List<double>();
            altitudeInMeters = new List<double>();
            altitudeInFeet = new List<double>();
            power = new List<double>();
            powerBalanceAndPedalling = new List<double>();
            TimeSpan time = new TimeSpan();
            dataTable = new DataTable();

            //set data table column types
            dataTable.Columns.Add("Heart Rate", typeof(double));
            dataTable.Columns.Add("Speed (km/h)", typeof(double));
            dataTable.Columns.Add("Speed (m/h)", typeof(double));
            dataTable.Columns.Add("Cadence", typeof(double));
            dataTable.Columns.Add("Altitude in meters", typeof(double));
            dataTable.Columns.Add("Altitude in feet", typeof(double));
            dataTable.Columns.Add("Power", typeof(double));
            dataTable.Columns.Add("Power And Peddaling Index", typeof(double));
            dataTable.Columns.Add("Peddling Index", typeof(int));
            dataTable.Columns.Add("Left Leg Balance", typeof(int));
            dataTable.Columns.Add("Right Leg Balance", typeof(int));
            dataTable.Columns.Add("Time", typeof(TimeSpan));

            //break smode into char array for processing data
            smodeIndex = this.SMode().ToCharArray();

            foreach (var line in Read()["[HRData]"])
            {
                
                string[] parts = Regex.Split(line, "\t");//split line using tab

                DataRow dataRow = dataTable.NewRow();

                if (paramsData["Interval"] == "238")
                {
                    heartRate.Add(60000 /Double.Parse(parts[0]));
                    dataRow["Heart Rate"] = 60000 / Double.Parse(parts[0]);
                }
                else
                {
                    heartRate.Add(Double.Parse(parts[0]));
                    dataRow["Heart Rate"] = Double.Parse(parts[0]);
                }

                int currentIteration = 1;

                if (smodeIndex[0].ToString().Equals("1"))
                {
                    dataRow["Speed (km/h)"] = Double.Parse(parts[currentIteration]) / 10;
                    dataRow["Speed (m/h)"] = Double.Parse(parts[currentIteration]) / 10 * 0.621371;
                    speedInKilos.Add(Double.Parse(parts[currentIteration]) / 10);
                    speedInMiles.Add(Double.Parse(parts[currentIteration]) / 10 * 0.621371);
                    currentIteration++;
                }

                if (smodeIndex[1].ToString().Equals("1"))
                {
                    dataRow["Cadence"] = Double.Parse(parts[currentIteration]);
                    cadence.Add(Double.Parse(parts[currentIteration]));
                    currentIteration++;
                }

                if (smodeIndex[2].ToString().Equals("1"))
                {
                    dataRow["Altitude in meters"] = Double.Parse(parts[currentIteration]);
                    dataRow["Altitude in feet"] = Double.Parse(parts[currentIteration]) * 3.28084;
                    altitudeInMeters.Add(Double.Parse(parts[currentIteration]));
                    altitudeInFeet.Add(Double.Parse(parts[currentIteration]) * 3.28084);
                    currentIteration++;
                }

                if (smodeIndex[3].ToString().Equals("1"))
                {
                    dataRow["Power"] = Double.Parse(parts[currentIteration]);
                    power.Add(Double.Parse(parts[currentIteration]));
                    currentIteration++;
                }

                if (smodeIndex[4].ToString().Equals("1"))
                {
                    dataRow["Power And Peddaling Index"] = Double.Parse(parts[currentIteration]);
                    powerBalanceAndPedalling.Add(Double.Parse(parts[currentIteration]));

                    string bits = AdvancedMetrics.ToBinary((int)Double.Parse(parts[currentIteration]));
                    int peddlingIndex = Convert.ToInt32(bits.Substring(0, 8), 2);
                    int leftLegBalance = Convert.ToInt32(bits.Substring(8), 2);
                    int rightLegBalance = 100 - leftLegBalance;

                    dataRow["Power And Peddaling Index"] = Double.Parse(parts[currentIteration]);
                    dataRow["Peddling Index"] = peddlingIndex;
                    dataRow["Left Leg Balance"] = leftLegBalance;
                    dataRow["Right Leg Balance"] = rightLegBalance;

                    currentIteration++;
                }

                dataRow["Time"] = time += TimeSpan.FromSeconds(1);

                dataTable.Rows.Add(dataRow);
            }
        }
    }
}
