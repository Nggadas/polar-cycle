﻿using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polar_Cycle
{
    public class Graph
    {
        public List<double> dataList { get; set; }
        public Axis syncedAxis { get; set; }

        public Graph(List<double> dataList, Axis syncedAxis)
        {
            this.dataList = dataList;
            this.syncedAxis = syncedAxis;
        }
    }
}
