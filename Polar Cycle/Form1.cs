﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.WinForms;
using LiveCharts.Geared;
using System.Windows.Media;
using Polar_Cycle;
using LiveCharts.Events;

namespace Polar_Cycle
{
    public partial class Form1 : Form
    {
        private string measurementUnit;
        private string filePath;
        private int currentGraph = 1;
        public List<double> subList;
        private Dictionary<int, Graph> indexedGraphs;
        private int maxSyncedAxisValue;
        private int minSyncedAxisValue;
        private FileReader fileReader;
        private double fullTrainingTime;



        public Form1()
        {
            InitializeComponent();
            tabControl1.Hide();
            measurementUnit = "EURO";
            uSToolStripMenuItem.Checked = false;
            euroToolStripMenuItem.Checked = true;
        }

        //contains all nessesary code for starting program
        public void StartProgram(string filePath)
        {
            indexedGraphs = new Dictionary<int, Graph>();
            tabControl1.Show();
            tabControl1.TabPages.Clear();
            tabControl1.TabPages.Add(tabPage1);
            tabControl1.TabPages.Add(tabPage2);

            ClearGraph(cartesianChart1);
            ClearGraph(cartesianChart2);
            ClearGraph(cartesianChart3);
            ClearGraph(cartesianChart4);
            ClearGraph(cartesianChart5);
            fileReader = new FileReader(filePath);
            fileReader.ParamsToList();
            fileReader.HrDatatoLists();

            //indexing graphs
            Graph heartRateGraph = new Graph(fileReader.heartRate,              new Axis());
            Graph speedGraphInKilos = new Graph(fileReader.speedInKilos,        new Axis());
            Graph speedGraphInMiles = new Graph(fileReader.speedInMiles,        new Axis());
            Graph cadence = new Graph(fileReader.cadence,                       new Axis());
            Graph altitueGraphInMeters = new Graph(fileReader.altitudeInMeters, new Axis());
            Graph altitueGraphInFeet = new Graph(fileReader.altitudeInFeet,     new Axis());
            Graph power = new Graph(fileReader.power,                           new Axis());


            indexedGraphs.Add(1, heartRateGraph);
            if (measurementUnit == "EURO")
            {
                indexedGraphs.Add(2, speedGraphInKilos);
                indexedGraphs.Add(4, altitueGraphInMeters);
            }
            else
            {
                indexedGraphs.Add(2, speedGraphInMiles);
                indexedGraphs.Add(4, altitueGraphInFeet);
            }
            indexedGraphs.Add(3, cadence);
            indexedGraphs.Add(5, power);


            CreateGraph(cartesianChart1, fileReader.heartRate, "Heart Rate", 1);

            //process each graph tab based on smode 
            if (fileReader.smodeIndex[0].ToString().Equals("1"))
            {
                tabControl1.TabPages.Add(tabPage3);
                if (measurementUnit == "EURO")
                {
                    CreateGraph(cartesianChart2, fileReader.speedInKilos, "Speed", 2);
                }
                else
                {
                    CreateGraph(cartesianChart2, fileReader.speedInMiles, "Speed", 2);
                }
            }
            else
            {
                tabControl1.TabPages.Remove(tabPage3);
            }

            if (fileReader.smodeIndex[1].ToString().Equals("1"))
            {
                tabControl1.TabPages.Add(tabPage4);
                CreateGraph(cartesianChart3, fileReader.cadence, "Cadence", 3);
            }
            else
            {
                tabControl1.TabPages.Remove(tabPage4);
            }

            if (fileReader.smodeIndex[2].ToString().Equals("1"))
            {
                tabControl1.TabPages.Add(tabPage5);
                if (measurementUnit == "EURO")
                {
                    CreateGraph(cartesianChart4, fileReader.altitudeInMeters, "Altitude", 4);
                }
                else
                {
                    CreateGraph(cartesianChart4, fileReader.altitudeInFeet, "Altitude", 4);
                }
            }
            else
            {
                tabControl1.TabPages.Remove(tabPage5);
            }

            if (fileReader.smodeIndex[3].ToString().Equals("1"))
            {
                tabControl1.TabPages.Add(tabPage6);
                CreateGraph(cartesianChart5, fileReader.power, "Power", 5);
            }
            else
            {
                tabControl1.TabPages.Remove(tabPage6);
            }

            dataGridView1.DataSource = fileReader.dataTable;

            //process speed and altitude datagridview columns based on measurment units
            if (measurementUnit == "EURO")
            {
                dataGridView1.Columns["Speed (km/h)"].Visible = true;
                dataGridView1.Columns["Speed (m/h)"].Visible = false;
                dataGridView1.Columns["Altitude in meters"].Visible = true;
                dataGridView1.Columns["Altitude in feet"].Visible = false;
            }
            else
            {
                dataGridView1.Columns["Speed (km/h)"].Visible = false;
                dataGridView1.Columns["Speed (m/h)"].Visible = true;
                dataGridView1.Columns["Altitude in meters"].Visible = false;
                dataGridView1.Columns["Altitude in feet"].Visible = true;
            }

            PopulateHeaderData();
            populateInterValDetectorsGrid();
        }

        //method for clearing graph 
        public void ClearGraph(LiveCharts.WinForms.CartesianChart cartesianChart)
        {
            cartesianChart.Series.Clear();
            cartesianChart.AxisX.Clear();
            cartesianChart.AxisY.Clear();
        }

        public void ResetSelection()
        {
            maxSyncedAxisValue = 0;
            minSyncedAxisValue = 0;
            indexedGraphs[currentGraph].syncedAxis.MinValue = Double.NaN;
            indexedGraphs[currentGraph].syncedAxis.MaxValue = Double.NaN;
            subList = indexedGraphs[currentGraph].dataList;
        }

        public void PopulateHeaderData()
        {
            //process label data based on measurement unit
            if (measurementUnit == "EURO")
            {
                if (fileReader.smodeIndex[0].ToString().Equals("1"))
                {
                    averageSpeedLabel.Text = Math.Round(indexedGraphs[2].dataList.Average(), 1).ToString();
                    maxSpeedLabelLabel.Text = Math.Round(indexedGraphs[2].dataList.Max(), 1).ToString();
                }
                else
                {
                    averageSpeedLabel.Text = "N/A";
                    maxSpeedLabelLabel.Text = "N/A";
                }

                if (fileReader.smodeIndex[2].ToString().Equals("1"))
                {
                    averageAltLabel.Text = Math.Round(indexedGraphs[4].dataList.Average(), 1).ToString();
                    maxAltLabel.Text = Math.Round(indexedGraphs[4].dataList.Max(), 1).ToString();
                }
                else
                {
                    averageAltLabel.Text = "N/A";
                    maxAltLabel.Text = "N/A";
                }
            }
            else
            {
                if (fileReader.smodeIndex[0].ToString().Equals("1"))
                {
                    averageSpeedLabel.Text = Math.Round(indexedGraphs[2].dataList.Average(), 1).ToString();
                    maxSpeedLabelLabel.Text = Math.Round(indexedGraphs[2].dataList.Max(), 1).ToString();
                }
                if (fileReader.smodeIndex[2].ToString().Equals("1"))
                {
                    averageAltLabel.Text = Math.Round(indexedGraphs[4].dataList.Average(), 1).ToString();
                    maxAltLabel.Text = Math.Round(indexedGraphs[4].dataList.Max(), 1).ToString();
                }
                else
                {
                    averageAltLabel.Text = "N/A";
                    maxAltLabel.Text = "N/A";
                }
            }

            if (fileReader.smodeIndex[3].ToString().Equals("1"))
            {
                averagePowerLabel.Text = Math.Round(indexedGraphs[5].dataList.Average(), 1).ToString();
                maxPowerLabel.Text = Math.Round(indexedGraphs[5].dataList.Max(), 1).ToString();
            }
            else
            {
                averagePowerLabel.Text = "N/A";
                maxPowerLabel.Text = "N/A";
            }
            //more label data
            averageHeartRateLabel.Text = Math.Round(indexedGraphs[1].dataList.Average(), 1).ToString();
            maxHeartRateLabel.Text = Math.Round(indexedGraphs[1].dataList.Max(), 1).ToString();
            minHeartRateLabel.Text = Math.Round(indexedGraphs[1].dataList.Min(), 1).ToString();

            //fetch individual time unit data and save as timespan
            var hours = Int32.Parse(fileReader.paramsData["Length"].Substring(0, 2));
            var minutes = Int32.Parse(fileReader.paramsData["Length"].Substring(3, 2));
            var seconds = Int32.Parse(fileReader.paramsData["Length"].Substring(6, 2));
            TimeSpan totalTime = new TimeSpan(hours, minutes, seconds);
            fullTrainingTime = totalTime.TotalSeconds;

            //calculate total distance covered 
            if (fileReader.smodeIndex[0].ToString().Equals("1"))
            {
                double averageSpeed = 0;
                if (measurementUnit == "EURO")
                {
                    averageSpeed = indexedGraphs[2].dataList.Average();
                }
                else
                {
                    averageSpeed = indexedGraphs[2].dataList.Average();
                }

                double totalDistance = Math.Round(averageSpeed * totalTime.TotalHours, 1);
                totalDistanceCoveredLabel.Text = totalDistance.ToString();

            }
            else
            {
                totalDistanceCoveredLabel.Text = "N/A";
            }
            if (fileReader.smodeIndex[3].ToString().Equals("1"))
            {
                npLabel.Text = AdvancedMetrics.NormalisedPower(fileReader.power, Int32.Parse(fileReader.paramsData["Interval"])).ToString();
            }
        }

        private void Axis_OnRangeChanged(RangeChangedEventArgs eventargs)
        {
            //TODO

            maxSyncedAxisValue = Convert.ToInt32(indexedGraphs[currentGraph].syncedAxis.ActualMaxValue);
            minSyncedAxisValue = Convert.ToInt32(indexedGraphs[currentGraph].syncedAxis.ActualMinValue);

            int maxGraphCount = Convert.ToInt32(indexedGraphs[currentGraph].dataList.Count);
            int minGraphCount = 0;

            if (maxSyncedAxisValue > maxGraphCount && minSyncedAxisValue > maxGraphCount)
            {
                ResetSelection();
            }
            else if (maxSyncedAxisValue < minGraphCount && minSyncedAxisValue < minGraphCount)
            {
                ResetSelection();
            }
            else if (maxSyncedAxisValue > maxGraphCount && minSyncedAxisValue < minGraphCount)
            {
                ResetSelection();
            }
            else if (maxSyncedAxisValue == minSyncedAxisValue)
            {
                ResetSelection();
            }
            else if (minSyncedAxisValue < minGraphCount)
            {
                subList = indexedGraphs[currentGraph].dataList.GetRange(0, maxSyncedAxisValue - 0);
            }
            else if (maxSyncedAxisValue > maxGraphCount)
            {
                subList = indexedGraphs[currentGraph].dataList.GetRange(minSyncedAxisValue, maxGraphCount - minSyncedAxisValue);
            }
            else
            {
                subList = indexedGraphs[currentGraph].dataList.GetRange(minSyncedAxisValue, maxSyncedAxisValue - minSyncedAxisValue);
            }
            indexedGraphs[currentGraph].dataList = subList;

            populateInterValDetectorsGrid();
        }


        //create cartesian chart using data from hrm file
        public void CreateGraph(LiveCharts.WinForms.CartesianChart cartesianChart, List<double> data, string label, int currentGraph)
        {
            cartesianChart.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Values = data.AsGearedValues(),
                    PointGeometry = null,
                    AreaLimit = 0,
                },
            };

            cartesianChart.AxisY.Add(new LiveCharts.Wpf.Axis
            {
                Title = label,
                LabelFormatter = value => value.ToString(),
            });

            indexedGraphs[currentGraph].syncedAxis.RangeChanged += Axis_OnRangeChanged;
            cartesianChart.AxisX.Add(indexedGraphs[currentGraph].syncedAxis);

            cartesianChart.Zoom = ZoomingOptions.X;
            cartesianChart.DisableAnimations = true;
            cartesianChart.LegendLocation = LegendLocation.None;

            cartesianChart.AxisX[0].MinValue = indexedGraphs[currentGraph].syncedAxis.MinValue;
            cartesianChart.AxisX[0].MaxValue = indexedGraphs[currentGraph].syncedAxis.MaxValue;

        }

        //load file for program
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "HRM|*.hrm";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                filePath = ofd.FileName;
                StartProgram(ofd.FileName);
            }
        }

        //set measurment unit to EURO
        private void euroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uSToolStripMenuItem.Checked = false;
            euroToolStripMenuItem.Checked = true;
            measurementUnit = "EURO";
            dataGridView1.Columns["Speed (km/h)"].Visible = true;
            dataGridView1.Columns["Speed (m/h)"].Visible = false;
            dataGridView1.Columns["Altitude in meters"].Visible = true;
            dataGridView1.Columns["Altitude in feet"].Visible = false;
            StartProgram(filePath);
        }

        //set measurment unit to US
        private void uSToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            euroToolStripMenuItem.Checked = false;
            uSToolStripMenuItem.Checked = true;
            measurementUnit = "US";
            dataGridView1.Columns["Speed (km/h)"].Visible = false;
            dataGridView1.Columns["Speed (m/h)"].Visible = true;
            dataGridView1.Columns["Altitude in meters"].Visible = false;
            dataGridView1.Columns["Altitude in feet"].Visible = true;
            StartProgram(filePath);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex >= 1)
            {
                currentGraph = tabControl1.SelectedIndex;
            }
            if (tabControl1.SelectedIndex == 0)
            {
                PopulateHeaderData();
            }
        }

        private void ftpTextBox_TextChanged(object sender, EventArgs e)
        {
            if (fileReader.smodeIndex[3].ToString().Equals("1"))
            {
                int ftp;
                bool isNumeric = int.TryParse(ftpTextBox.Text, out ftp);
                int interval = Int32.Parse(fileReader.paramsData["Interval"]);
                double normalizedPower = AdvancedMetrics.NormalisedPower(fileReader.power, interval);

                if (!string.IsNullOrWhiteSpace(ftpTextBox.Text))
                {
                    if (isNumeric)
                    {
                        double intensityFactor = AdvancedMetrics.IntensityFactor(normalizedPower, ftp);
                        double tss = AdvancedMetrics.TrainingStressScore(fullTrainingTime, normalizedPower, intensityFactor, ftp);

                        ifLabel.Text = intensityFactor.ToString();
                        tssLabel.Text = tss.ToString();
                    }
                    else
                    {
                        ftpTextBox.ResetText();
                    }
                }
            }
        }

        public void populateInterValDetectorsGrid()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("starting", typeof(double));
            dataTable.Columns.Add("ending", typeof(double));
            dataTable.Columns.Add("time", typeof(double));

            List<InterValDetector> interValDetectors = AdvancedMetrics.InterValDetector(indexedGraphs[currentGraph].dataList, 0);

            foreach (var interval in interValDetectors)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["starting"] = interval.starting;
                dataRow["ending"] = interval.ending;
                dataRow["time"] = interval.time;
                dataTable.Rows.Add(dataRow);
            }

            dataGridView2.DataSource = dataTable;
        }
    }
}
