﻿namespace Polar_Cycle
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitConversionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.euroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.cartesianChart5 = new LiveCharts.WinForms.CartesianChart();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.cartesianChart4 = new LiveCharts.WinForms.CartesianChart();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cartesianChart3 = new LiveCharts.WinForms.CartesianChart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cartesianChart2 = new LiveCharts.WinForms.CartesianChart();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.ftpTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tssLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ifLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.npLabel = new System.Windows.Forms.Label();
            this.averageAltLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.maxAltLabel = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.maxHeartRateLabel = new System.Windows.Forms.Label();
            this.averageHeartRateLabel = new System.Windows.Forms.Label();
            this.maxSpeedLabelLabel = new System.Windows.Forms.Label();
            this.averagePowerLabel = new System.Windows.Forms.Label();
            this.minHeartRateLabel = new System.Windows.Forms.Label();
            this.maxPowerLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.averageSpeedLabel = new System.Windows.Forms.Label();
            this.totalDistanceCoveredLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.menuStrip1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.unitConversionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1358, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // unitConversionToolStripMenuItem
            // 
            this.unitConversionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.euroToolStripMenuItem,
            this.uSToolStripMenuItem});
            this.unitConversionToolStripMenuItem.Name = "unitConversionToolStripMenuItem";
            this.unitConversionToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.unitConversionToolStripMenuItem.Text = "Unit Conversion";
            // 
            // euroToolStripMenuItem
            // 
            this.euroToolStripMenuItem.Name = "euroToolStripMenuItem";
            this.euroToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.euroToolStripMenuItem.Text = "Euro";
            this.euroToolStripMenuItem.Click += new System.EventHandler(this.euroToolStripMenuItem_Click);
            // 
            // uSToolStripMenuItem
            // 
            this.uSToolStripMenuItem.Name = "uSToolStripMenuItem";
            this.uSToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.uSToolStripMenuItem.Text = "US";
            this.uSToolStripMenuItem.Click += new System.EventHandler(this.uSToolStripMenuItem_Click_1);
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPage6.Controls.Add(this.cartesianChart5);
            this.tabPage6.Controls.Add(this.dataGridView2);
            this.tabPage6.ForeColor = System.Drawing.Color.Snow;
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1350, 696);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Power";
            // 
            // cartesianChart5
            // 
            this.cartesianChart5.Hoverable = true;
            this.cartesianChart5.Location = new System.Drawing.Point(27, 23);
            this.cartesianChart5.Name = "cartesianChart5";
            this.cartesianChart5.ScrollHorizontalFrom = 0D;
            this.cartesianChart5.ScrollHorizontalTo = 0D;
            this.cartesianChart5.ScrollMode = LiveCharts.ScrollMode.None;
            this.cartesianChart5.ScrollVerticalFrom = 0D;
            this.cartesianChart5.ScrollVerticalTo = 0D;
            this.cartesianChart5.Size = new System.Drawing.Size(1297, 419);
            this.cartesianChart5.TabIndex = 5;
            this.cartesianChart5.Text = "cartesianChart5";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.MenuText;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Snow;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridView2.Location = new System.Drawing.Point(634, 448);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(358, 185);
            this.dataGridView2.TabIndex = 4;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPage5.Controls.Add(this.cartesianChart4);
            this.tabPage5.ForeColor = System.Drawing.Color.Snow;
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1350, 696);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Altitude";
            // 
            // cartesianChart4
            // 
            this.cartesianChart4.Hoverable = true;
            this.cartesianChart4.Location = new System.Drawing.Point(19, 6);
            this.cartesianChart4.Name = "cartesianChart4";
            this.cartesianChart4.ScrollHorizontalFrom = 0D;
            this.cartesianChart4.ScrollHorizontalTo = 0D;
            this.cartesianChart4.ScrollMode = LiveCharts.ScrollMode.None;
            this.cartesianChart4.ScrollVerticalFrom = 0D;
            this.cartesianChart4.ScrollVerticalTo = 0D;
            this.cartesianChart4.Size = new System.Drawing.Size(1297, 596);
            this.cartesianChart4.TabIndex = 1;
            this.cartesianChart4.Text = "cartesianChart4";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPage4.Controls.Add(this.cartesianChart3);
            this.tabPage4.ForeColor = System.Drawing.Color.Snow;
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1350, 696);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Cadence";
            // 
            // cartesianChart3
            // 
            this.cartesianChart3.Hoverable = true;
            this.cartesianChart3.Location = new System.Drawing.Point(18, 19);
            this.cartesianChart3.Name = "cartesianChart3";
            this.cartesianChart3.ScrollHorizontalFrom = 0D;
            this.cartesianChart3.ScrollHorizontalTo = 0D;
            this.cartesianChart3.ScrollMode = LiveCharts.ScrollMode.None;
            this.cartesianChart3.ScrollVerticalFrom = 0D;
            this.cartesianChart3.ScrollVerticalTo = 0D;
            this.cartesianChart3.Size = new System.Drawing.Size(1297, 596);
            this.cartesianChart3.TabIndex = 1;
            this.cartesianChart3.Text = "cartesianChart3";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPage3.Controls.Add(this.cartesianChart2);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.ForeColor = System.Drawing.Color.Snow;
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1350, 696);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Speed";
            // 
            // cartesianChart2
            // 
            this.cartesianChart2.Hoverable = true;
            this.cartesianChart2.Location = new System.Drawing.Point(23, 6);
            this.cartesianChart2.Name = "cartesianChart2";
            this.cartesianChart2.ScrollHorizontalFrom = 0D;
            this.cartesianChart2.ScrollHorizontalTo = 0D;
            this.cartesianChart2.ScrollMode = LiveCharts.ScrollMode.None;
            this.cartesianChart2.ScrollVerticalFrom = 0D;
            this.cartesianChart2.ScrollVerticalTo = 0D;
            this.cartesianChart2.Size = new System.Drawing.Size(1297, 596);
            this.cartesianChart2.TabIndex = 4;
            this.cartesianChart2.Text = "cartesianChart2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(633, 253);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "There is no data";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPage2.Controls.Add(this.cartesianChart1);
            this.tabPage2.ForeColor = System.Drawing.Color.Snow;
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1350, 696);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Heart rate";
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Hoverable = true;
            this.cartesianChart1.Location = new System.Drawing.Point(32, 20);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.ScrollHorizontalFrom = 0D;
            this.cartesianChart1.ScrollHorizontalTo = 0D;
            this.cartesianChart1.ScrollMode = LiveCharts.ScrollMode.None;
            this.cartesianChart1.ScrollVerticalFrom = 0D;
            this.cartesianChart1.ScrollVerticalTo = 0D;
            this.cartesianChart1.Size = new System.Drawing.Size(1297, 596);
            this.cartesianChart1.TabIndex = 0;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.ftpTextBox);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.tssLabel);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.ifLabel);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.npLabel);
            this.tabPage1.Controls.Add(this.averageAltLabel);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.maxAltLabel);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.maxHeartRateLabel);
            this.tabPage1.Controls.Add(this.averageHeartRateLabel);
            this.tabPage1.Controls.Add(this.maxSpeedLabelLabel);
            this.tabPage1.Controls.Add(this.averagePowerLabel);
            this.tabPage1.Controls.Add(this.minHeartRateLabel);
            this.tabPage1.Controls.Add(this.maxPowerLabel);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.averageSpeedLabel);
            this.tabPage1.Controls.Add(this.totalDistanceCoveredLabel);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.ForeColor = System.Drawing.Color.Black;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1350, 696);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Home";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Snow;
            this.label16.Location = new System.Drawing.Point(874, 404);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(301, 22);
            this.label16.TabIndex = 17;
            this.label16.Text = "Functional Threshhold power:";
            // 
            // ftpTextBox
            // 
            this.ftpTextBox.Location = new System.Drawing.Point(1196, 404);
            this.ftpTextBox.Name = "ftpTextBox";
            this.ftpTextBox.Size = new System.Drawing.Size(128, 20);
            this.ftpTextBox.TabIndex = 16;
            this.ftpTextBox.TextChanged += new System.EventHandler(this.ftpTextBox_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Snow;
            this.label14.Location = new System.Drawing.Point(874, 543);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(217, 22);
            this.label14.TabIndex = 14;
            this.label14.Text = "Training Stress Core:";
            // 
            // tssLabel
            // 
            this.tssLabel.AutoSize = true;
            this.tssLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tssLabel.ForeColor = System.Drawing.Color.Snow;
            this.tssLabel.Location = new System.Drawing.Point(1127, 543);
            this.tssLabel.Name = "tssLabel";
            this.tssLabel.Size = new System.Drawing.Size(42, 24);
            this.tssLabel.TabIndex = 15;
            this.tssLabel.Text = "N/A";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Snow;
            this.label8.Location = new System.Drawing.Point(874, 491);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(176, 22);
            this.label8.TabIndex = 12;
            this.label8.Text = "Intensity Factor:";
            // 
            // ifLabel
            // 
            this.ifLabel.AutoSize = true;
            this.ifLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ifLabel.ForeColor = System.Drawing.Color.Snow;
            this.ifLabel.Location = new System.Drawing.Point(1127, 489);
            this.ifLabel.Name = "ifLabel";
            this.ifLabel.Size = new System.Drawing.Size(42, 24);
            this.ifLabel.TabIndex = 13;
            this.ifLabel.Text = "N/A";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Snow;
            this.label6.Location = new System.Drawing.Point(874, 443);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(190, 22);
            this.label6.TabIndex = 10;
            this.label6.Text = "Normalised power:";
            // 
            // npLabel
            // 
            this.npLabel.AutoSize = true;
            this.npLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.npLabel.ForeColor = System.Drawing.Color.Snow;
            this.npLabel.Location = new System.Drawing.Point(1127, 443);
            this.npLabel.Name = "npLabel";
            this.npLabel.Size = new System.Drawing.Size(42, 24);
            this.npLabel.TabIndex = 11;
            this.npLabel.Text = "N/A";
            // 
            // averageAltLabel
            // 
            this.averageAltLabel.AutoSize = true;
            this.averageAltLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageAltLabel.ForeColor = System.Drawing.Color.Snow;
            this.averageAltLabel.Location = new System.Drawing.Point(728, 507);
            this.averageAltLabel.Name = "averageAltLabel";
            this.averageAltLabel.Size = new System.Drawing.Size(42, 24);
            this.averageAltLabel.TabIndex = 6;
            this.averageAltLabel.Text = "N/A";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Snow;
            this.label18.Location = new System.Drawing.Point(479, 507);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(177, 22);
            this.label18.TabIndex = 7;
            this.label18.Text = "Average altitude:";
            // 
            // maxAltLabel
            // 
            this.maxAltLabel.AutoSize = true;
            this.maxAltLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxAltLabel.ForeColor = System.Drawing.Color.Snow;
            this.maxAltLabel.Location = new System.Drawing.Point(728, 541);
            this.maxAltLabel.Name = "maxAltLabel";
            this.maxAltLabel.Size = new System.Drawing.Size(42, 24);
            this.maxAltLabel.TabIndex = 8;
            this.maxAltLabel.Text = "N/A";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Snow;
            this.label20.Location = new System.Drawing.Point(479, 541);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(189, 22);
            this.label20.TabIndex = 9;
            this.label20.Text = "Maximum altitude";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Snow;
            this.label13.Location = new System.Drawing.Point(479, 437);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(158, 22);
            this.label13.TabIndex = 5;
            this.label13.Text = "Average power:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Snow;
            this.label12.Location = new System.Drawing.Point(479, 402);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(214, 22);
            this.label12.TabIndex = 5;
            this.label12.Text = "Minimum heart rate:";
            // 
            // maxHeartRateLabel
            // 
            this.maxHeartRateLabel.AutoSize = true;
            this.maxHeartRateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxHeartRateLabel.ForeColor = System.Drawing.Color.Snow;
            this.maxHeartRateLabel.Location = new System.Drawing.Point(306, 541);
            this.maxHeartRateLabel.Name = "maxHeartRateLabel";
            this.maxHeartRateLabel.Size = new System.Drawing.Size(42, 24);
            this.maxHeartRateLabel.TabIndex = 5;
            this.maxHeartRateLabel.Text = "N/A";
            // 
            // averageHeartRateLabel
            // 
            this.averageHeartRateLabel.AutoSize = true;
            this.averageHeartRateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageHeartRateLabel.ForeColor = System.Drawing.Color.Snow;
            this.averageHeartRateLabel.Location = new System.Drawing.Point(306, 507);
            this.averageHeartRateLabel.Name = "averageHeartRateLabel";
            this.averageHeartRateLabel.Size = new System.Drawing.Size(42, 24);
            this.averageHeartRateLabel.TabIndex = 5;
            this.averageHeartRateLabel.Text = "N/A";
            // 
            // maxSpeedLabelLabel
            // 
            this.maxSpeedLabelLabel.AutoSize = true;
            this.maxSpeedLabelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSpeedLabelLabel.ForeColor = System.Drawing.Color.Snow;
            this.maxSpeedLabelLabel.Location = new System.Drawing.Point(306, 472);
            this.maxSpeedLabelLabel.Name = "maxSpeedLabelLabel";
            this.maxSpeedLabelLabel.Size = new System.Drawing.Size(42, 24);
            this.maxSpeedLabelLabel.TabIndex = 5;
            this.maxSpeedLabelLabel.Text = "N/A";
            // 
            // averagePowerLabel
            // 
            this.averagePowerLabel.AutoSize = true;
            this.averagePowerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averagePowerLabel.ForeColor = System.Drawing.Color.Snow;
            this.averagePowerLabel.Location = new System.Drawing.Point(728, 437);
            this.averagePowerLabel.Name = "averagePowerLabel";
            this.averagePowerLabel.Size = new System.Drawing.Size(42, 24);
            this.averagePowerLabel.TabIndex = 5;
            this.averagePowerLabel.Text = "N/A";
            // 
            // minHeartRateLabel
            // 
            this.minHeartRateLabel.AutoSize = true;
            this.minHeartRateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minHeartRateLabel.ForeColor = System.Drawing.Color.Snow;
            this.minHeartRateLabel.Location = new System.Drawing.Point(728, 402);
            this.minHeartRateLabel.Name = "minHeartRateLabel";
            this.minHeartRateLabel.Size = new System.Drawing.Size(42, 24);
            this.minHeartRateLabel.TabIndex = 5;
            this.minHeartRateLabel.Text = "N/A";
            // 
            // maxPowerLabel
            // 
            this.maxPowerLabel.AutoSize = true;
            this.maxPowerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxPowerLabel.ForeColor = System.Drawing.Color.Snow;
            this.maxPowerLabel.Location = new System.Drawing.Point(728, 472);
            this.maxPowerLabel.Name = "maxPowerLabel";
            this.maxPowerLabel.Size = new System.Drawing.Size(42, 24);
            this.maxPowerLabel.TabIndex = 5;
            this.maxPowerLabel.Text = "N/A";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Snow;
            this.label11.Location = new System.Drawing.Point(479, 472);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(176, 22);
            this.label11.TabIndex = 5;
            this.label11.Text = "Maximum power:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Snow;
            this.label9.Location = new System.Drawing.Point(32, 541);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(217, 22);
            this.label9.TabIndex = 5;
            this.label9.Text = "Maximum heart rate:";
            // 
            // averageSpeedLabel
            // 
            this.averageSpeedLabel.AutoSize = true;
            this.averageSpeedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageSpeedLabel.ForeColor = System.Drawing.Color.Snow;
            this.averageSpeedLabel.Location = new System.Drawing.Point(306, 437);
            this.averageSpeedLabel.Name = "averageSpeedLabel";
            this.averageSpeedLabel.Size = new System.Drawing.Size(42, 24);
            this.averageSpeedLabel.TabIndex = 5;
            this.averageSpeedLabel.Text = "N/A";
            // 
            // totalDistanceCoveredLabel
            // 
            this.totalDistanceCoveredLabel.AutoSize = true;
            this.totalDistanceCoveredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalDistanceCoveredLabel.ForeColor = System.Drawing.Color.Snow;
            this.totalDistanceCoveredLabel.Location = new System.Drawing.Point(306, 402);
            this.totalDistanceCoveredLabel.Name = "totalDistanceCoveredLabel";
            this.totalDistanceCoveredLabel.Size = new System.Drawing.Size(42, 24);
            this.totalDistanceCoveredLabel.TabIndex = 5;
            this.totalDistanceCoveredLabel.Text = "N/A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Snow;
            this.label4.Location = new System.Drawing.Point(32, 507);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 22);
            this.label4.TabIndex = 4;
            this.label4.Text = "Average heart rate:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(32, 472);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "Maximum speed:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Snow;
            this.label2.Location = new System.Drawing.Point(32, 437);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "Average speed:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(32, 402);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Total distance covered:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridView1.Location = new System.Drawing.Point(117, 18);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1111, 352);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(12, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1358, 722);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.ClientSize = new System.Drawing.Size(1358, 691);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitConversionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem euroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uSToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label averageAltLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label maxAltLabel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label maxHeartRateLabel;
        private System.Windows.Forms.Label averageHeartRateLabel;
        private System.Windows.Forms.Label maxSpeedLabelLabel;
        private System.Windows.Forms.Label averagePowerLabel;
        private System.Windows.Forms.Label minHeartRateLabel;
        private System.Windows.Forms.Label maxPowerLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label averageSpeedLabel;
        private System.Windows.Forms.Label totalDistanceCoveredLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox ftpTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label tssLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label ifLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label npLabel;
        private System.Windows.Forms.DataGridView dataGridView2;
        private LiveCharts.WinForms.CartesianChart cartesianChart5;
        private LiveCharts.WinForms.CartesianChart cartesianChart4;
        private LiveCharts.WinForms.CartesianChart cartesianChart3;
        private LiveCharts.WinForms.CartesianChart cartesianChart2;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
    }
}

